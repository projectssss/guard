importScripts('https://www.gstatic.com/firebasejs/5.0.4/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.0.4/firebase-messaging.js');

// Firebase App is always required and must be first
var config = {
    apiKey: "AIzaSyD2bp1lZ4L32Sqj1lDh5jqOAsqZLRm9lLE",
    authDomain: "teleguardgbr.firebaseapp.com",
    projectId: "teleguardgbr",
    messagingSenderId: "972375599368",
  };
  firebase.initializeApp(config);

  const messaging = firebase.messaging();
  messaging.setBackgroundMessageHandler(function(payload) {
if(payload.data.key < 0){
  const title = 'Отмена вызова от' + payload.data.sender_id;
  const options = {
    body: 'ГБР снова свободна:' + payload.data.receiver_id
  };
}else{
  const title = 'Поступил новый вызов от' + payload.data.sender_id;
  const options = {
    body: 'Вызов приянт ГБР:' + payload.data.receiver_id
  };
}
    return self.registration.showNotification(title, options);
  });
