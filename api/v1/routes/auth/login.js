var okResult = {
    code    : 200,
    token   : '',
    role    : '',
    message : "Authentication successful"
};

const failedResult = {
    code: 502,
    message: "Internal server error"
};

const tooManyAttemptsResult = {
    code: 429,
    message: "Too many requests"
};

const unauthorizedResult = {
    code: 401,
    message: "Invalid username or password"
};

const utf8 = require('utf8');
const pg = require('pg');
const pgConfig = require('../dbconnection.js');

var jwt    = require('jsonwebtoken');
const jwtSecret = require('./jwtconfig').secret;

module.exports = {
    loginUser : function(req, res){
        var login = req.query.login;
        var pwd = req.query.password;
        var fbToken = req.query.fbtoken;

        if(null == login){
            login = req.params.login;
        }
        if(null == pwd){
            pwd = req.params.password;
        }
        if(null == fbToken){
            fbToken = req.params.fbtoken;
        }

        //console.log('Login: ' + login);
        //console.log('Password: ' + pwd);
        //console.log('Firebase token: ' + fbToken);

        var pool = new pg.Pool(pgConfig);
        pool.connect(function(err,client,done) {
           if(err){
               failedResult.message = err;
               res.status(502).send(failedResult);
           }
           client.query('SELECT * from authentication_login($1, $2, $3)', [login, pwd, fbToken], function(err, result) {
               if(err){
                   failedResult.message = err;
                   res.status(502).send(failedResult);
               }
               var checkResult = result.rows[0];
               console.log(result);
               if('' == checkResult.urole){
                   done();
                   console.log("empty role");
                   res.status(401).send(unauthorizedResult);
               } else {
                   okResult.role = checkResult.urole;
                   var jwtTokenData = {
                       id   : checkResult.userid,
                       role : checkResult.urole
                   };
                   var token = jwt.sign(jwtTokenData, jwtSecret, {
                       expiresIn: 4320000 // expires in 30 days
                   });
                   okResult.token = token;
                   client.query('SELECT * from authentication_save_jwt($1, $2)', [checkResult.userid, token], function(err,result) {
                       if(err){
                           console.log(err);
                           res.status(502).send(err);
                       }
                       var checkRes = result.rows[0];
                       if(0 == checkRes.authentication_save_jwt){
                           done();
                           res.status(502).json(failedResult);
                       } else {
                           done();
                           res.status(200).json(okResult);
                       }
                   });
               }
           });
        });
    }
}
