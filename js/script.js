$(function(){
  var myFirebaseToken;
  // password = "A152E841783914146E4BCD4F39100686"; // TODO: пароль будет браться из поля value убрать
  // Firebase App is always required and must be first
  var config = {
      apiKey: "AIzaSyD2bp1lZ4L32Sqj1lDh5jqOAsqZLRm9lLE",
      authDomain: "teleguardgbr.firebaseapp.com",
      projectId: "teleguardgbr",
      messagingSenderId: "972375599368",
    };
    firebase.initializeApp(config);

    const messaging = firebase.messaging();
    messaging.requestPermission()
    .then(function() {
      console.log('Have permission');
      return messaging.getToken();
    })
    .then(function(token) {
      console.log(token);
      myFirebaseToken = token;
      setCookie('fbtoken', token);
    })
    .catch(function(err) {
      console.log('Error 0ccured.');
    })

    messaging.onMessage(function(payload) {
      console.log('onMassege: ', payload);
    });
  $('#enter-btn').click(function(e){
    e.preventDefault();
   var password = $('#enter-pass').val();
   var login = $('#enter-login').val();
   password = hex_md5(password);
   console.log(password);

   var query = 'https://yyurchenko.ddns.ukrtel.net/api/v1/auth/login?login=' + login + '&password=' + password + '&fbtoken=' + myFirebaseToken;
    $.ajax({
      type: "POST",
      url: query,
      // data: {
      //   login: "disp-demo",
      //   password: $('#enter-pass').val(),
      //   // password: "A152E841783914146E4BCD4F39100686",
      //   fbtoken: "myFirebaseTokenOrSpace"
      // },
      success: function(data) {
        console.log(data.code);
        if(data.code == 200 && data.message == 'Authentication successful'){
          setCookie('password', password);
          setCookie('login', login);
          window.location.href = "https://yyurchenko.ddns.ukrtel.net/home.html";
        }else{
          console.log('login error' + data.code);
          $('.user').text('Попробуйте снова');
        }
      }
    }).fail(function() {
      console.log('login error');
    $('.user').text('Попробуйте снова');
  });
  });

  function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
      var d = new Date();
      d.setTime(d.getTime() + expires * 1000);
      expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
      options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
      updatedCookie += "; " + propName;
      var propValue = options[propName];
      if (propValue !== true) {
        updatedCookie += "=" + propValue;
      }
    }

    document.cookie = updatedCookie;
  }
});
// {
//   "code": 200,
//   "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEzNiIsInJvbGUiOiJkaXNwYXRjaGVyIiwiaWF0IjoxNTI4MDMwNTg0LCJleHAiOjE1MzIzNTA1ODR9.3-PdLRjtmCDdknhINu4dpTz8mz5TAZ4e-fR-t-5u6_w",
//   "role": "dispatcher",
//   "message": "Authentication successful"
// }
