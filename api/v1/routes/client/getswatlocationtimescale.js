var okResult = {
    code: 200,
    scale : 0,
    swat_id : 0,
    longitude: 0,
    latitude: 0,
    distance: 0,
    message: 'OK'
};

const failedResult = {
    code: 502,
    message: "Internal server error"
};

var md5 = require('md5');
const pg = require('pg');
const pgConfig = require('../dbconnection.js');

module.exports = {
    getSwatLocationAndScale : function(req, res){
        var hour = req.query.hour;
        var lon = req.query.lon;
        var lat = req.query.lat;

        var pool = new pg.Pool(pgConfig);
        pool.connect(function(err,client,done) {
           if(err){
               console.log("not able to get connection " + err);
               res.status(502).send(err);
           }
           client.query('SELECT * from client_get_swat_location_time_scale($1, $2, $3)', [hour, lon, lat], function(err,result) {
               if(err){
                   console.log(err);
                   res.status(502).send(err);
               }
               okResult.scale = result.rows[0].scale;
               okResult.swat_id = result.rows[0].swat_id;
               okResult.longitude = result.rows[0].longitude;
               okResult.latitude = result.rows[0].latitude;
               okResult.distance = result.rows[0].distance;
               done();
               if(0 == okResult.id){
                   res.status(502).send("Location and scale querying error");
               } else {
                   res.status(200).send(okResult);
               }
           });
        });
    }
}
