var express = require('express');
var router = express.Router();

var authCtl = require('./auth/auth.js');
var auxCtl = require('./aux/aux.js');
var clientCtl = require('./client/client.js');
var guardCtl = require('./guard/guard.js');
var webCtl = require('./web/web.js');

/****************************************************************** AUTH **********************************************************/



/**
 * @swagger
 * /auth/login:
 *   post:
 *     description: Log in and get verification SMS onto registered users's phone
 *     tags:
 *       - Authentication
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: login
 *         description: user's login
 *         in: query
 *         required: true
 *         type: string
 *         maxLength: 20
 *       - name: password
 *         description: password's MD5 hash
 *         in: query
 *         required: true
 *         type: string
 *         maxLength: 32
 *       - name: fbtoken
 *         description: user's Firebase token
 *         in: query
 *         required: true
 *         type: string
 *         maxLength: 320
 *     responses:
 *       200:
 *         description: user's login and password are correct and SMS verification code has been sent successfully
 *       401:
 *         description: login and/or password are incorrect
 *       502:
 *         description: internal server error, something wrong with database connection or server logic
 */
router.post('/auth/login', function(req, res){
    authCtl.login(req, res);
});


/****************************************************************** AUX **********************************************************/

/**
 * @swagger
 * /aux/getjwtcheck:
 *   get:
 *     description: Check JWT, is it expired
 *     tags:
 *       - Aux
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: x-device-token
 *         description: device token hash
 *         in: header
 *         required: true
 *         schema:
 *            type: string
 *            maxLength: 32
 *       - name: x-access-token
 *         description: Json web token
 *         in: header
 *         required: true
 *         schema:
 *            type: string
 *            maxLength: 1024
 *     responses:
 *       200:
 *         description: OK, data returned
 *       401:
 *         description: access denied, authentication data missed
 *       502:
 *         description: internal server error, something wrong with database connection or server logic
 */
router.get('/aux/getjwtcheck', function(req, res){
    auxCtl.checkJwt(req, res);
});

/****************************************************************** CLIENT **********************************************************/



/**
 * @swagger
 * /client/registerclient:
 *   post:
 *     description: Log in and get verification SMS onto registered users's phone
 *     tags:
 *       - Client
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: clientid
 *         description: client's application instance Id
 *         in: query
 *         required: true
 *         type: string
 *         maxLength: 255
 *       - name: fbtoken
 *         description: firebase token
 *         in: query
 *         required: true
 *         type: string
 *         maxLength: 320
 *     responses:
 *       200:
 *         description: client id has been updated successfully
 *       502:
 *         description: internal server error, something wrong with database connection or server logic
 */
router.post('/client/registerclient', function(req, res){
    clientCtl.registerClient(req, res);
});

/**
 * @swagger
 * /client/gettimescale:
 *   get:
 *     description: get time scale for guard driving
 *     tags:
 *       - Client
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: hour
 *         description: day hour number
 *         in: query
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: everything ok, scale returned
 *       502:
 *         description: internal server error, something wrong with database connection or server logic
 */
router.get('/client/gettimescale', function(req, res){
    clientCtl.getTimeScale(req, res);
});

/**
 * @swagger
 * /client/getswatlocationandtimescale:
 *   get:
 *     description: get nearest swat location and time scale
 *     tags:
 *       - Client
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: hour
 *         description: day hour number
 *         in: query
 *         required: true
 *         type: integer
 *       - name: lon
 *         description: client's longitude
 *         in: query
 *         required: true
 *         type: number
 *       - name: lat
 *         description: client's latitude
 *         in: query
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: everything ok, scale returned
 *       502:
 *         description: internal server error, something wrong with database connection or server logic
 */
router.get('/client/getswatlocationandtimescale', function(req, res){
    clientCtl.getSwatLocationAndTimeScale(req, res);
});

/**
 * @swagger
 * /client/sendalarm:
 *   post:
 *     description: send alarm to swat
 *     tags:
 *       - Client
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: clientid
 *         description: client id
 *         in: query
 *         required: true
 *         type: integer
 *       - name: swatid
 *         description: swat command id
 *         in: query
 *         required: true
 *         type: integer
 *       - name: timestamp
 *         description: call timestamp
 *         in: query
 *         required: true
 *         type: number
 *       - name: longitude
 *         description: client's longitude
 *         in: query
 *         required: true
 *         type: number
 *       - name: latitude
 *         description: client's latitude
 *         in: query
 *         required: true
 *         type: number
 *       - name: fbtoken
 *         description: client's Firebase token
 *         in: query
 *         required: true
 *         type: string
 *         maxLength: 320
 *     responses:
 *       200:
 *         description: everything ok, call sent
 *       502:
 *         description: internal server error, something wrong with database connection or server logic
 */
router.post('/client/sendalarm', function(req, res){
    clientCtl.sendAlarm(req, res);
});

/**
 * @swagger
 * /client/cancelalarm:
 *   post:
 *     description: send alarm to swat
 *     tags:
 *       - Client
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: clientid
 *         description: client id
 *         in: query
 *         required: true
 *         type: integer
 *       - name: swatid
 *         description: swat command id
 *         in: query
 *         required: true
 *         type: integer
 *       - name: alarmid
 *         description: alarm's id to cancel
 *         in: query
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: everything ok, call canceled
 *       502:
 *         description: internal server error, something wrong with database connection or server logic
 */
router.post('/client/cancelalarm', function(req, res){
    clientCtl.cancelAlarm(req, res);
});

/****************************************************************** GUARD **********************************************************/



/**
 * @swagger
 * /guard/sendLastLocation:
 *   post:
 *     description: Send last SWAT location
 *     tags:
 *       - Guard
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: x-access-token
 *         description: Json web token
 *         in: header
 *         required: true
 *         schema:
 *            type: string
 *            maxLength: 1024
 *       - name: longitude
 *         description: Location longitude
 *         in: query
 *         required: true
 *         type: number
 *       - name: latitude
 *         description: Location latitude
 *         in: query
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: Location updated successful
 *       401:
 *         description: login and/or password are incorrect
 *       502:
 *         description: internal server error, something wrong with database connection or server logic
 */
router.post('/guard/sendLastLocation', function(req, res){
    guardCtl.sendGuardLocation(req, res);
});


/****************************************************************** WEB **********************************************************/



/**
 * @swagger
 * /web/getguardslastlocations:
 *   get:
 *     description: Send last SWATs locations
 *     tags:
 *       - Web
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: x-access-token
 *         description: Json web token
 *         in: header
 *         required: true
 *         schema:
 *            type: string
 *            maxLength: 1024
 *     responses:
 *       200:
 *         description: Location updated successful
 *       401:
 *         description: login and/or password are incorrect
 *       502:
 *         description: internal server error, something wrong with database connection or server logic
 */
router.get('/web/getguardslastlocations', function(req, res){
    webCtl.getGuardsLocations(req, res);
});


/****************************************************************** MISC **********************************************************/

/**
 * @swagger
 * /:
 *   get:
 *     description: Retrieve the full list of nothing
 *     tags:
 *       - nothing
 *     produces:
 *       - text/html
 *     responses:
 *       200:
 *         description: none
 */
router.get('/', function (req, res) {
  res.status(200).send('Teleguard API');
});

/**
 * @swagger
 * /version:
 *   get:
 *     description: Retrieve the api version
 *     tags:
 *       - nothing
 *     produces:
 *       - text/html
 *     responses:
 *       200:
 *         description: no description
 */
router.get('/version', function(req, res){
    var okResult = {
        code: 200,
        version: "1.0.0",
        message: "OK"
    };
    res.status(200).json(okResult);
});

module.exports = router;
