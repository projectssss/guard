var okResult = {
    code: 200,
    newid : 0,
    message: 'OK'
};

const invalidEventIdResult = {
    code: 401,
    message: "Invalid event id"
};

const accessDeniedResult = {
    code: 401,
    message: "Invalid credentials"
};

const accessExpired = {
    code: 401,
    message: "jwt expired"
};

const failedResult = {
    code: 502,
    message: "Internal server error"
};

var md5 = require('md5');
const pg = require('pg');
const pgConfig = require('../dbconnection.js');

var jwt    = require('jsonwebtoken');
const jwtSecret = require('../auth/jwtconfig').secret;

var RestClient = require('node-rest-client').Client;

const fbConfig = require('../firebaseparamsGbr.js');

module.exports = {
    sendAlarmToSwat : function(req, res){
        var clientId = req.query.clientid;
        var swatId = req.query.swatid;
        var timestamp = req.query.timestamp;
        var longitude = req.query.longitude;
        var latitude = req.query.latitude;
        var fbtoken = req.query.fbtoken;
        var callstate = 1; // start swat call

        var pool = new pg.Pool(pgConfig);
        pool.connect(function(err,client,done) {
           if(err){
               console.log("not able to get connection " + err);
               res.status(502).send(err);
           }
           client.query('SELECT * from messages_send_alarm($1, $2, $3, $4, $5, $6, $7)', [swatId, timestamp, longitude, latitude, clientId, callstate, fbtoken], function(err1, result1) {
               if(err1){
                   console.log(err1);
                   res.status(502).send(err1);
               } else {
                   var alarmid = result1.rows[0].alarm_id;
                   var swatFbtoken = result1.rows[0].swat_fb_token;
                   var dispFbtoken = result1.rows[0].disp_fb_token;
                   var fbRequest = {
                                data : {
                                    key : alarmid,
                                    type : 1,                // alarm notification
                                    sender_id : clientId,
                                    receiver_id : swatId,
                                    time : timestamp,
                                    lon : longitude,
                                    lat : latitude,
                                    state : callstate
                                },
                                to : swatFbtoken
                             };
                   var args = {
                                  data: fbRequest,
                                  headers: {
                                      "Content-Type" : "application/json",
                                      "Authorization": fbConfig.authKey
                                  }
                             };
                   var restSmsClient = new RestClient();
                   restSmsClient.post(fbConfig.serviceUrl, args, function (data, response) {
                        /*if(Buffer.isBuffer(data)){
                            data = data.toString('utf8');
                        }
                        console.log(data); // { message_id: 5871365907699591000 } */

                        if((null != dispFbtoken) && (64 < dispFbtoken.length)){
                            fbRequest.to = dispFbtoken;
                            args.data = fbRequest;
                            restSmsClient.post(fbConfig.serviceUrl, args, function (data11, response11) {
                                 // Do nothing here
                            });
                        }
                        okResult.newid = alarmid;
                        res.status(200).json(okResult);
                   });
               }
           });
        });
    }
}
