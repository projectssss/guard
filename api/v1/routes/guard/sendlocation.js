var okResult = {
    code: 200,
    message: 'OK'
};

const accessDeniedResult = {
    code: 401,
    message: "Invalid credentials"
};

const accessExpired = {
    code: 401,
    message: "jwt expired"
};

const failedResult = {
    code: 502,
    message: "Internal server error"
};

var md5 = require('md5');
const pg = require('pg');
const pgConfig = require('../dbconnection.js');

var jwt    = require('jsonwebtoken');
const jwtSecret = require('../auth/jwtconfig').secret;

module.exports = {
    sendSwatLocation : function(req, res){
        var firebaseTokenDeviceHash = req.get('x-device-token'); // from header
        var jwtToken = req.get('x-access-token');                // from header too
        var lon = req.query.longitude;
        var lat = req.query.latitude;

        var pool = new pg.Pool(pgConfig);

        if(!(jwtToken && firebaseTokenDeviceHash)){
            console.log('one of tokens or both are missing');
            res.status(401).json(accessDeniedResult);
        }
        jwt.verify(jwtToken, jwtSecret, function(err, decoded) {
            if (err) {
                console.log(err.message);
                res.status(401).json(accessExpired);
            } else {
                // if everything is good, save to request for use in other routes
                var jwtDecoded = decoded;
                //console.log(jwtDecoded);
                if("gbr" != jwtDecoded.role){
                    console.log("Invalid role");
                    res.status(401).json(accessExpired);
                } else {
                    pool.connect(function(err,client,done) {
                       if(err){
                           console.log("not able to get connection " + err);
                           res.status(502).send(err);
                       }
                       client.query('SELECT * from guard_update_location($1, $2, $3)', [jwtDecoded.id, lon, lat], function(err,result) {
                           if(err){
                               console.log(err);
                               res.status(502).send(err);
                           }
                           done();
                           res.status(200).json(okResult);
                       });
                    });
                }
            }
        });
    }
}
