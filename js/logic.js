$(function(){
  // Объявляем переменные map и infoWindow за пределами функции initMap,
  // тем самым делая их глобальными и теперь мы их можем использовать внутри любой функции, а не только внутри initMap, как это было раньше.
  var map, infoWindow;
  var gbrMarkers = [];
  var clientAlarmMarkers = [];
  var alarmCouner = 0;
  var password = getCookie('password');
  var myFirebaseToken = getCookie('fbtoken');
  var login = getCookie('login');
  var appToken;
  // password = 'A152E841783914146E4BCD4F39100686'; // TODO: убрать пароль будет браться из кук
  var query = 'https://yyurchenko.ddns.ukrtel.net/api/v1/auth/login?login=' + login + '&password=' + password + '&fbtoken=' + myFirebaseToken;

     $.ajax({
       type: "POST",
       url: query

     }).fail(function() {
       console.log('error data');
   }).done(function( data ) {
     console.log(data.code);
     if(data.code == 200 && data.message == 'Authentication successful'){
       console.log(data.token);
       setCookie('token', data.token);
       appToken = data.token;
       startMap();
       getGuardPosition(data.token);
       var timerId = setInterval(function() {
  getGuardPosition(data.token);
}, 120000);
       // getGuardPosition(data.token);
     }else{
       console.log('error data');
       window.location.href = "https://yyurchenko.ddns.ukrtel.net/index.html";
     }
  });


var config = {
    apiKey: "AIzaSyD2bp1lZ4L32Sqj1lDh5jqOAsqZLRm9lLE",
    authDomain: "teleguardgbr.firebaseapp.com",
    projectId: "teleguardgbr",
    messagingSenderId: "972375599368",
  };
  firebase.initializeApp(config);

  const messaging = firebase.messaging();
  messaging.requestPermission()
  .then(function() {
    console.log('Have permission');
    return messaging.getToken();
  })
  .then(function(token) {
    console.log(token);
  })
  .catch(function(err) {
    console.log('Error 0ccured.');
  })

  messaging.onMessage(function(payload) {
    console.log('onMassege: ', payload);
    //Проверяем есть ли вызовы в массиве вызовов
    if(payload.data.key < 0){
    if(clientAlarmMarkers.length != 0){
      //Смотрим пришла ли отмена вызова
        console.log('отмена');
        for (var i = 0; i < clientAlarmMarkers.length; i++){
          //Находим нужный вызов  на который пришла отмена
          if(clientAlarmMarkers[i].key == payload.data.key * (-1)){
            //Удаляем с карты метку
            clientAlarmMarkers[i].marker.setMap(null);
            //Удаляем из массива
            clientAlarmMarkers.splice(i, 1);
          }
        }
        //Если пришла отмена вызова удаляем из панели уведомлений вызовов
        $( ".cancel-gbr-btn" ).each(function( index ) {
          if($(this).data('key') == payload.data.key * (-1)){
            console.log('удаляемый элемент', $(this).parent().parent().parent().parent());
            $(this).parent().parent().parent().parent().remove();
          }
});
      }
    }else{
        console.log('новый вызов');
        // console.log(payload.data.sender_id);
        var latLng = new google.maps.LatLng(payload.data.lat, payload.data.lon);
        var name = payload.data.sender_id;
        var gbrId =  payload.data.receiver_id;

    var mapOptions = {
    zoom: 10,
    center: latLng
    }

    var marker = new google.maps.Marker({
      position: latLng,
      label: 'A',
      animation: google.maps.Animation.DROP,
      title: 'Вызов от' + name
    });
    var alarmObj = {
      key: payload.data.key,
      marker: marker
    }

clientAlarmMarkers.push(alarmObj);
alarmCouner++;
//Добавление элемента на панель вызовов
var newCallHTMLTemplate =
// "<div class='panel panel-default'>" +
"<div class='panel-heading'>" +
  "<h4 class='panel-title'>" +
    "<a class='collapsed' data-toggle='collapse' data-parent='#accordion' href='#collapse" + alarmCouner + "' aria-expanded='false'>" +
      "Новый вызов ГБР <i class='fa fa-bolt fa-gbr'></i></a></h4></div>" +
"<div id='collapse" + alarmCouner + "' class='panel-collapse collapse' aria-expanded='false' style='height: 0px;'>" +
  "<div class='panel-body'>" +
    "<div class='alarm-main-info'>" +
      "<div class='gbr'>ГБР принявшая вызов:<span></span></div>" +
      "<div class='client-id'>Клиент вызвавший:<span></span></div>" +
      "<button type='button' data-key='' data-clientid='' data-swatid='' class='btn btn-primary btn-cons cancel-gbr-btn'>Отмена вызова</button>" +
"</div></div></div>";

var $newAlarm = $( "<div class='panel panel-default'></div>" );

$('.alarm-panel').append($newAlarm);
$('.panel-default').last().html(newCallHTMLTemplate);
$('.panel-default').last().find('.gbr span').text(gbrId);
$('.panel-default').last().find('.client-id span').text(name);
$('.panel-default').last().find('.cancel-gbr-btn').data("key", payload.data.key);
$('.panel-default').last().find('.cancel-gbr-btn').data("clientid", payload.data.sender_id);
$('.panel-default').last().find('.cancel-gbr-btn').data("swatid", payload.data.receiver_id);

    // Отслеживаем клик по нашему маркеру
    google.maps.event.addListener(marker, "click", function() {
      // contentString - это переменная в которой хранится содержимое информационного окна.
      var contentString = '<div class="infowindow">' +
                              '<h3>Вызов от:' + name + '</h3>' +
                              '<p>ГБР выехавшая на вызов:' + gbrId + '</p>' +
                          '</div>';
      // Меняем содержимое информационного окна
      infoWindow.setContent(contentString);
      // Показываем информационное окно
      infoWindow.open(map, marker);
    });

    // To add the marker to the map, call setMap();
    marker.setMap(map);




    }

  });

  //Закрытие уведомления отсутствия ГБР
  $('.btn-messege-close').click(function () {
    $('#noticfaction').addClass('messenger-empty');
    $('#noticfaction .messenger-message').addClass('messenger-hidden');
  });

  //Отмена вызова по кнопке
  $('.alarm-panel').on('click', '.cancel-gbr-btn', function(e) {
  // $('.cancel-gbr-btn').click(function () {
    console.log('cancel btn', $(this).data( "key" ));
    var key = $(this).data( "key" );
    for (var i = 0; i < clientAlarmMarkers.length; i++){
      //Находим нужный вызов  на который пришла отмена
      if(clientAlarmMarkers[i].key == key){
        //Удаляем с карты метку
        clientAlarmMarkers[i].marker.setMap(null);
        //Удаляем из массива
        clientAlarmMarkers.splice(i, 1);
      }
    }
    var clientId = $(this).data( "clientid" );
    var swatId = $(this).data( "swatid" );
    $(this).parent().parent().parent().parent().remove();

    var cancelAlarmQuery = 'https://yyurchenko.ddns.ukrtel.net/api/v1/client/cancelalarm?clientid=' + clientId + '&swatid=' + swatId + '&alarmid=' + key;
    $.ajax({
      type: "POST",
      url: cancelAlarmQuery,
      success: function(data) {
        if(data.code == 200){
          console.log('вызов отменен', data);
        }else if(data.code == 502){
          console.log('internal server error, something wrong with database connection or server logic');
          console.log(data);
        }
      }
    }).fail(function() {
      console.log('internal server error, something wrong with database connection or server logic');
  });

  });

function startMap(){
  var centerLatLng = new google.maps.LatLng(56.2928515, 43.7866641);
  var mapOptions = {
      center: centerLatLng,
      zoom: 7
  };
  map = new google.maps.Map(document.getElementById("map"), mapOptions);
  // Создаем объект информационного окна и помещаем его в переменную infoWindow
  // Так как у каждого информационного окна свое содержимое, то создаем пустой объект, без передачи ему параметра content
  infoWindow = new google.maps.InfoWindow();
  // Отслеживаем клик в любом месте карты
  google.maps.event.addListener(map, "click", function() {
      // infoWindow.close - закрываем информационное окно.
      infoWindow.close();
  });
}

// function initilizeMap(markersData){

  function initilizeMap(markersData) {

      // Определяем границы видимой области карты в соответствии с положением маркеров
    var bounds = new google.maps.LatLngBounds();
      // Перебираем в цикле все координата хранящиеся в markersData
      for (var i = 0; i < markersData.length; i++){
          var latLng = new google.maps.LatLng(markersData[i].latitude, markersData[i].longitude);
          var name = markersData[i].swat_login;
          var id = markersData[i].swat_id;
          // Добавляем маркер с информационным окном
          addMarker(latLng, name, id);
          // Расширяем границы нашей видимой области, добавив координаты нашего текущего маркера
       bounds.extend(latLng);
      }

      // Автоматически масштабируем карту так, чтобы все маркеры были в видимой области карты
 map.fitBounds(bounds);
 map.setZoom(10);
  }
//   google.maps.event.addDomListener(window, "load", initMap);
// }



  // Функция добавления маркера с информационным окном
  function addMarker(latLng, name, id) {
      var marker = new google.maps.Marker({
          position: latLng,
          map: map,
          title: name
      });
      var newGBR = {
        gbrID :id,
        marker: marker
      }
      gbrMarkers.push(newGBR);
      // Отслеживаем клик по нашему маркеру
      google.maps.event.addListener(marker, "click", function() {
          // contentString - это переменная в которой хранится содержимое информационного окна.
          var contentString = '<div class="infowindow">' +
                                  '<h3>Логин:' + name + '</h3>' +
                                  '<p>ID:' + id + '</p>' +
                              '</div>';
          // Меняем содержимое информационного окна
          infoWindow.setContent(contentString);
          // Показываем информационное окно
          infoWindow.open(map, marker);
      });
  }

function refreshGbrPosition(markersData) {
  // Определяем границы видимой области карты в соответствии с положением маркеров
var bounds = new google.maps.LatLngBounds();
  // Перебираем в цикле все координаты хранящиеся в markersData
  for (var i = 0; i < markersData.length; i++){
    gbrMarkers.forEach(function(element, index) {
      // console.log('gbrMarkers.length - ',gbrMarkers.length);
        var latLng = new google.maps.LatLng(markersData[i].latitude, markersData[i].longitude);
        //Проверяем совпадают ли координаты
        if(markersData[i].swat_id == element.gbrID){

          //При несовпадении удаляем старую метку с карты
          element.marker.setMap(null);
          var name = markersData[i].swat_login;
          var id = markersData[i].swat_id;
          //Ставим новую метку
          var marker = new google.maps.Marker({
              position: latLng,
              map: map,
              title: name
          });
          var newGBR = {
            gbrID :id,
            marker: marker
          }
          gbrMarkers[index] = newGBR;
          // Отслеживаем клик по нашему маркеру
          google.maps.event.addListener(marker, "click", function() {
              // contentString - это переменная в которой хранится содержимое информационного окна.
              var contentString = '<div class="infowindow">' +
                                      '<h3>Логин:' + name + '</h3>' +
                                      '<p>ID:' + id + '</p>' +
                                  '</div>';
              // Меняем содержимое информационного окна
              infoWindow.setContent(contentString);
              // Показываем информационное окно
              infoWindow.open(map, marker);
          });
          // Добавляем маркер с информационным окном
          addMarker(latLng, name, id);
          // Расширяем границы нашей видимой области, добавив координаты нашего текущего маркера
          bounds.extend(latLng);

        }else{
          var name = markersData[i].swat_login;
          var id = markersData[i].swat_id;
          //Ставим новую метку
          var marker = new google.maps.Marker({
              position: latLng,
              map: map,
              title: name
          });
          var newGBR = {
            gbrID :id,
            marker: marker
          }
          gbrMarkers.push(newGBR);
          // Отслеживаем клик по нашему маркеру
          google.maps.event.addListener(marker, "click", function() {
              // contentString - это переменная в которой хранится содержимое информационного окна.
              var contentString = '<div class="infowindow">' +
                                      '<h3>Логин:' + name + '</h3>' +
                                      '<p>ID:' + id + '</p>' +
                                  '</div>';
              // Меняем содержимое информационного окна
              infoWindow.setContent(contentString);
              // Показываем информационное окно
              infoWindow.open(map, marker);
        });

        // Добавляем маркер с информационным окном
        addMarker(latLng, name, id);
        // Расширяем границы нашей видимой области, добавив координаты нашего текущего маркера
     bounds.extend(latLng);

   }
      console.log(element);
    });

  // Автоматически масштабируем карту так, чтобы все маркеры были в видимой области карты
map.fitBounds(bounds);
map.setZoom(10);

}
}


//Получаем локацию всех ГБР
function getGuardPosition(token){
  if( getCookie('token') != undefined){
    var queryGuardPosition = 'https://yyurchenko.ddns.ukrtel.net/api/v1/web/getguardslastlocations';
    $.ajax({
      beforeSend: function(xhrObj){
               xhrObj.setRequestHeader("x-access-token", token);
       },
      type: "GET",
      url: queryGuardPosition,
      // headers: {"x-access-token" : token},
      success: function(data) {
        if(data.code == 200){
          console.log(data);
          // initialize();
          //Проверяем приходит ли инфо о ГБР с сервера, в случае успеха выводим метки на карту, иначе ничего не выводим
          if(data.data.length > 0){
            $('#noticfaction').addClass('messenger-empty');
            $('#noticfaction .messenger-message').addClass('messenger-hidden');
            if(gbrMarkers.length > 0){
                refreshGbrPosition(data.data);
                console.log('map Refresh');

            }else{
              initilizeMap(data.data);
              console.log('map init');
            }
          }else{
            if(gbrMarkers.length > 0){
              // Перебираем в цикле все координата хранящиеся в markersData
              for (var i = 0; i < gbrMarkers.length; i++){
                    gbrMarkers[i].marker.setMap(null);
                }
                gbrMarkers = [];
                gbrMarkers.length = 0;
                //вывести инфо
                console.log('No GBR');
                console.log('Remove Markers');
                $('#noticfaction').removeClass('messenger-empty');
                $('#noticfaction .messenger-message').removeClass('messenger-hidden');
            }else{
              //вывести инфо
              console.log('No GBR');
              $('#noticfaction').removeClass('messenger-empty');
              $('#noticfaction .messenger-message').removeClass('messenger-hidden');
            }
          }


          // initMap(lat, lang, swatId);
        }else if(data.code == 401){
          console.log('login and/or password are incorrect');
          console.log(data);
        }else{
          console.log('internal server error, something wrong with database connection or server logic');
          console.log(data);
        }
      }
    }).fail(function() {
      console.log('error no location');

  });
  }else{
    console.log('token cookies not found');
  }
}


// возвращает cookie с именем name, если есть, если нет, то undefined
function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}


//Установка куки
function setCookie(name, value, options) {
  options = options || {};

  var expires = options.expires;

  if (typeof expires == "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires * 1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);

  var updatedCookie = name + "=" + value;

  for (var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];
    if (propValue !== true) {
      updatedCookie += "=" + propValue;
    }
  }

  document.cookie = updatedCookie;
}

});
