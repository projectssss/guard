var okResult = {
    code: 200,
    scale : 0,
    message: 'OK'
};

const failedResult = {
    code: 502,
    message: "Internal server error"
};

var md5 = require('md5');
const pg = require('pg');
const pgConfig = require('../dbconnection.js');

module.exports = {
    getScale : function(req, res){
        var hour = req.query.hour;

        var pool = new pg.Pool(pgConfig);
        pool.connect(function(err,client,done) {
           if(err){
               console.log("not able to get connection " + err);
               res.status(502).send(err);
           }
           client.query('SELECT * from client_get_time_scale($1)', [hour], function(err,result) {
               if(err){
                   console.log(err);
                   res.status(502).send(err);
               }
               okResult.scale = result.rows[0].client_get_time_scale;
               done();
               if(0 == okResult.id){
                   res.status(502).send("Scale querying error");
               } else {
                   res.status(200).send(okResult);
               }
           });
        });
    }
}
