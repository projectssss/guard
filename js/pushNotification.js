  // Firebase App is always required and must be first
  var config = {
      apiKey: "AIzaSyD2bp1lZ4L32Sqj1lDh5jqOAsqZLRm9lLE",
      authDomain: "teleguardgbr.firebaseapp.com",
      projectId: "teleguardgbr",
      messagingSenderId: "972375599368",
    };
    firebase.initializeApp(config);

    const messaging = firebase.messaging();
    messaging.requestPermission()
    .then(function() {
      console.log('Have permission');
      return messaging.getToken();
    })
    .then(function(token) {
      console.log(token);
    })
    .catch(function(err) {
      console.log('Error 0ccured.');
    })

    messaging.onMessage(function(payload) {
      console.log('onMassege: ', payload);
      console.log(payload.data.sender_id);
      var latLng = new google.maps.LatLng(payload.data.lat, payload.data.lon);
      var name = payload.data.sender_id;
      var gbrId =  payload.data.receiver_id;

var mapOptions = {
  zoom: 10,
  center: latLng
}
var map = new google.maps.Map(document.getElementById("map"), mapOptions);

var marker = new google.maps.Marker({
    position: latLng,
    title: 'Вызов от' + name
});

// Отслеживаем клик по нашему маркеру
google.maps.event.addListener(marker, "click", function() {
    // contentString - это переменная в которой хранится содержимое информационного окна.
    var contentString = '<div class="infowindow">' +
                            '<h3>Вызов от:' + name + '</h3>' +
                            '<p>ГБР выехавшая на вызов:' + gbrId + '</p>' +
                        '</div>';
    // Меняем содержимое информационного окна
    infoWindow.setContent(contentString);
    // Показываем информационное окно
    infoWindow.open(map, marker);
});

// To add the marker to the map, call setMap();
marker.setMap(map);
    });
