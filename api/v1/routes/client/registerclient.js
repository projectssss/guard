var okResult = {
    code: 200,
    newid : 0,
    message: 'OK'
};

const accessDeniedResult = {
    code: 401,
    message: "Invalid credentials"
};

const accessExpired = {
    code: 401,
    message: "jwt expired"
};

const failedResult = {
    code: 502,
    message: "Internal server error"
};

var md5 = require('md5');
const pg = require('pg');
const pgConfig = require('../dbconnection.js');

module.exports = {
    registerClient : function(req, res){
        var clientId = req.query.clientid;
        var fbToken = req.query.fbtoken;

        var pool = new pg.Pool(pgConfig);
        pool.connect(function(err,client,done) {
           if(err){
               console.log("not able to get connection " + err);
               res.status(502).send(err);
           }
           client.query('SELECT * from client_update_client_id_fb_token($1, $2)', [clientId, fbToken], function(err,result) {
               if(err){
                   console.log(err);
                   res.status(502).send(err);
               }
               okResult.newid = result.rows[0].client_update_client_id_fb_token;
               done();
               if(0 == okResult.newid){
                   res.status(502).send("Client resgistration server error");
               } else {
                   res.status(200).send(okResult);
               }
           });
        });
    }
}
