var okResult = {
    code: 200,
    message: 'OK'
};

const invalidEventIdResult = {
    code: 401,
    message: "Invalid event id"
};

const accessDeniedResult = {
    code: 401,
    message: "Invalid credentials"
};

const accessExpired = {
    code: 401,
    message: "jwt expired"
};

const failedResult = {
    code: 502,
    message: "Internal server error"
};

var md5 = require('md5');
const pg = require('pg');
const pgConfig = require('../dbconnection.js');

var jwt    = require('jsonwebtoken');
const jwtSecret = require('../auth/jwtconfig').secret;

var RestClient = require('node-rest-client').Client;

const fbConfig = require('../firebaseparamsGbr.js');
const fbClientConfig = require('../firebaseparamsClient.js');

module.exports = {
    cancelAlarmToSwat : function(req, res){
        var alarmId = req.query.alarmid;
        var clientId = req.query.clientid;
        var swatId = req.query.swatid;
        var callstate = 2; // cancel swat call

        if(0 == alarmId){
            res.status(200).json(okResult);
        } else {
            var pool = new pg.Pool(pgConfig);
            pool.connect(function(err,client,done) {
               if(err){
                   console.log("not able to get connection " + err);
                   res.status(502).send(err);
               }
               client.query('SELECT * from messages_cancel_alarm($1, $2)', [swatId, alarmId], function(err1, result1) {
                   if(err1){
                       console.log(err1);
                       res.status(502).send(err1);
                   } else {
                       var swatFbtoken = result1.rows[0].swat_fb_token;
                       var dispFbtoken = result1.rows[0].disp_fb_token;
                       var clientFbToken = result1.rows[0].client_fb_token;

                       console.log("alarmId: " + alarmId);

                       var fbRequest = {
                                    data : {
                                        key : -1 * alarmId,
                                        type : 2,                // cancelalarm notification
                                        sender_id : clientId,


                                        receiver_id : swatId,
                                        time : 0,
                                        lon : 0,
                                        lat : 0,
                                        state : callstate
                                    },
                                    to : swatFbtoken
                                 };
                       var args = {
                                      data: fbRequest,
                                      headers: {
                                          "Content-Type" : "application/json",
                                          "Authorization": fbConfig.authKey
                                      }
                                 };
                       var argsClient = {
                                        data: fbRequest,
                                        headers: {
                                            "Content-Type" : "application/json",
                                            "Authorization": fbClientConfig.authKey
                                        }
                                  };
                       var restSmsClient = new RestClient();
                       restSmsClient.post(fbConfig.serviceUrl, args, function (data, response) {
                            /*if(Buffer.isBuffer(data)){
                                data = data.toString('utf8');
                            }
                            //console.log(data); */ // { message_id: 5871365907699591000 }
                            if((null != dispFbtoken) && (64 < dispFbtoken.length)){
                                fbRequest.to = dispFbtoken;
                                args.data = fbRequest;
                                restSmsClient.post(fbConfig.serviceUrl, args, function (data11, response11) {
                                     if((null != clientFbToken) && (64 < clientFbToken.length)){
                                         fbRequest.to = clientFbToken;
                                         argsClient.data = fbRequest;
                                         restSmsClient.post(fbClientConfig.serviceUrl, argsClient, function (data111, response111) {
                                             // Do nothing
                                         });
                                     }
                                });
                            }
                            res.status(200).json(okResult);
                       });
                   }
               });
            });
        }
    }
}
