var okResult = {
    code: 200,
    data: null,
    message: 'OK'
};

const accessDeniedResult = {
    code: 401,
    message: "Invalid credentials"
};

const accessExpired = {
    code: 401,
    message: "jwt expired"
};

const failedResult = {
    code: 502,
    message: "Internal server error"
};

var md5 = require('md5');
const pg = require('pg');
const pgConfig = require('../dbconnection.js');

var jwt    = require('jsonwebtoken');
const jwtSecret = require('../auth/jwtconfig').secret;

module.exports = {
    getSwatsLocations : function(req, res){
        //var firebaseTokenDeviceHash = req.get('x-device-token'); // from header
        var jwtToken = req.get('x-access-token');

        console.log('getSwatsLocations JWT: ' + jwtToken);


        var pool = new pg.Pool(pgConfig);

        if((null == jwtToken) || ('' == jwtToken)){
            console.log('JWT token is missing');
            res.status(401).json(accessDeniedResult);
        }
        jwt.verify(jwtToken, jwtSecret, function(err, decoded) {
            if (err) {
                console.log(err.message);
                res.status(401).json(accessExpired);
            } else {
                // if everything is good, save to request for use in other routes
                var jwtDecoded = decoded;
                //console.log(jwtDecoded);
                if("dispatcher" != jwtDecoded.role){
                    console.log("Invalid role");
                    res.status(401).json(accessExpired);
                } else {
                    pool.connect(function(err,client,done) {
                       if(err){
                           console.log("not able to get connection " + err);
                           res.status(502).send(err);
                       }
                       client.query('SELECT * from web_get_swats()', [], function(err,result) {
                           if(err){
                               console.log(err);
                               res.status(502).send(err);
                           }
                           okResult.data = result.rows;
                           done();
                           res.status(200).json(okResult);
                       });
                    });
                }
            }
        });
    }
}
