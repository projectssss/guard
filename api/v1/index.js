/*

TeleguardClient

API     Key     AIzaSyDzGfvUWZFzTCtLAgzBbped-3hJitFf2ho
Android Key     AIzaSyDlQfn9CRgDjzc5bfxjMCC2XlRhVAh3e5k
Server  Key     AIzaSyB8jCM3u1Ng2n14m8rcKF-DQOiyrVOlXQI
Browser Key     AIzaSyAX4ZDQSucNTEYOmacFr7Yk5StW-6tKetI

*/


const options = {
  swaggerDefinition: {
    info: {
      title: 'Teleguard REST API',
      version: '1.0.0',
      description: 'Teleguard is mobile application for companies safety and guarding',
      contact: {
        email: 'null@dev.com'
      }
    },
    tags: [
      {
        name: 'Teleguard API',
        description: 'all server API for application'
      }
    ],
    schemes: ['https'],
    host: 'yyurchenko.ddns.ukrtel.net',
    basePath: '/api/v1/'
  },
  apis: ['./routes/router.js']
};


var express = require('express');
var bodyParser = require('body-parser');
var router = require('./routes/router.js');
var urlencodedParser = bodyParser.urlencoded({ extended: true });

var http = require('http');
var fileUpload = require('express-fileupload');
var fs = require('fs');
var path = require('path');

var firebase = require("firebase");

var app = express();
app.use(bodyParser.json());
app.use(urlencodedParser);
app.use(fileUpload());

const swaggerJSDoc = require('swagger-jsdoc');
var swaggerUi = require('swagger-ui-express');
const swaggerSpec = swaggerJSDoc(options);

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
app.use('/api/v1', router);

app.listen(8091, function () {
   console.log("Teleguard Node.JS services started");
});

module.exports = app;
