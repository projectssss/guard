
var pgConnection = {
  user : 'postgres',
  database : 'guard',
  password : 'postgres',
  host : 'localhost',
  port : 5432,
  max : 20, // max number of connection can be open to database
  idleTimeoutMillis : 9000 // how long a client is allowed to remain idle before being closed
};

module.exports = pgConnection;
