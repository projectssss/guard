var regClient = require('./registerclient');
var scaleTime = require('./gettimescale');
var swatLocScale = require('./getswatlocationtimescale');
var sendAlarm = require('./sendalarm');
var cancelAlarm = require('./cancelalarm');

module.exports = {
   registerClient : function(req, res){
       regClient.registerClient(req, res);
   },
   getTimeScale : function(req, res){
       scaleTime.getScale(req, res);
   },
   getSwatLocationAndTimeScale : function(req, res){
       swatLocScale.getSwatLocationAndScale(req, res);
   },
   sendAlarm : function(req, res){
       sendAlarm.sendAlarmToSwat(req, res);
   },
   cancelAlarm : function(req, res){
       cancelAlarm.cancelAlarmToSwat(req, res);
   }
}
